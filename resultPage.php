<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <title>Result Page</title>
</head>

<body>

<?php
            
            $x = $_POST['xVar'];
            $y = $_POST['yVar'];

            if(!isset($_POST['operator'])){
                echo("Select an Operator");
                exit;
            }
            $operator = $_POST['operator'];
            if($x==""){
                echo("Select a First Value");
                exit;
            }
            if($y==""){
                echo("Select a Second Value");
                exit;
            }
            if($operator == "Addition"){
                $result = $x+$y;
                echo($x. "+" . $y . "=" . $result);
                exit;
            }
            if($operator == "Subtraction"){
                $result = $x-$y;
                echo($x. "-" . $y . "=" . $result);
                exit;
            }
            if($operator == "Multiply"){
                $result = $x*$y;
                echo($x. "*" . $y . "=" . $result);
                exit;
            }
            if($operator == "Divide"){
                if($y == "0"){
                    echo("Can't Divide by 0");
                    exit;
                }
                else{
                    $result = $x/$y;
                    echo($x. "/" . $y . "=" . $result);
                    exit;
                }
            }
?>

</body>
</html>
